(ns mastermindmalinbaum.fonctions2)

;=========================================
; fonctions du Solveur (Codeur, partie 2)
;=========================================

(defn ecrire_nos_indic
  "affichage consignes"
  []
  (println "-------------- Ecrire : good, color, bad  "))

(defn ecrire_vecteur
  "affichage vecteur 1"
  [v1]
  (loop [i 0]
    (when (< i 4)
      (print (str (name (nth v1 i)) " "))
      (recur (inc i)))))

(defn ecrire_vecteur1
    "affichage vecteur 2"
    [v1]
    (println "Choix de l'Ordinateur :")
    (println "-----------------------")
    (loop [i 0]
       (when (< i 4)
             (print (str (name (nth v1 i)) " "))
             (recur (inc i)))))

(defn ecrire_gagne_partie2
     "affichage victoire"
     [v1]
     (when (= v1 [:good :good :good :good])
       (println "!!! L'ORDINATEUR a GAGNE !!!")
       (System/exit 0)))

(defn place
  "fixe place good dans un vecteur"
  [vordi vindic vplace]
  (loop [i 0 res []]
    (if (< i 4)
      (if (= (nth vplace i) :none)
        (if (= (nth vindic i) :good)
          (recur (inc i) (conj res (nth vordi i)))
          (recur (inc i) (conj res :none))))
      res)))

(defn enlever_couleur
  "enleve un element dans un vecteur"
  [couleur vecteur]
  (remove #{couleur} vecteur))

(defn boite_couleur
    "retourne les couleurs possibles, pour une indication bad"
    [vordi vindic]
    (loop [i 0 res [:rouge :bleu :vert :jaune :noir :blanc]]
      (if (< i 4)
        (if (= (nth vindic i) :bad)
          (recur (inc i) (enlever_couleur (nth vordi i) res))
          res))))

(defn generateur_dyn
     "genere des couleurs possibles sur les places libres"
     [vordi vindic vplace]
     (loop [i 0 res []]
         (if (< i 4)
            (if (= (nth (place vordi vindic vplace) i) :none)
               (recur (inc i) (conj res (rand-nth (boite_couleur vordi vindic))))
               (recur (inc i)(conj res (nth vordi i))))
           res)))
