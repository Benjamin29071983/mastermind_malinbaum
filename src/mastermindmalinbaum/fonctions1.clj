(ns mastermindmalinbaum.fonctions1)

(declare generateur)
(declare reajustement)
(declare indications)
(declare frequences)
(declare contient)

;=========================================
; fonctions du Decodeur (partie 1)
;=========================================

(defn generateur
  "creation de 4 variables, compose aleatoirement des 6 couleurs"
  []
  (def gen1
        (rand-nth [:rouge :bleu :vert :jaune :noir :blanc]))

  (def gen2
         (rand-nth [:rouge :bleu :vert :jaune :noir :blanc]))

  (def gen3
         (rand-nth [:rouge :bleu :vert :jaune :noir :blanc]))

  (def gen4
          (rand-nth [:rouge :bleu :vert :jaune :noir :blanc])))

(defn contient
  "teste si x, est une valeur d'un vecteur"
  [x v]
  (loop [s v, i 0]
    (if (seq s)
      (if (= (first s) x)
        true
        (recur (rest s) (inc i))))))

(defn frequences
   "le map de la frequence des couleurs"
   [v1]
   (loop [s v1, res {}]
      (if (seq s)
        (if (get res (first s))
            (recur (rest s) (assoc res (first s) (+ 1 (get res (first s)))))
            (recur (rest s) (assoc res (first s) 1)))
        res)))

(defn indications
      "donne des indications sur les couleurs attendues"
      [v1 v2 freq1]
      (loop [s v2, i 0, f1 freq1, res []]
            (if (seq s)
               (cond
                  (= (nth v1 i) (first s)) (recur (rest s) (inc i)  (assoc f1 (first s) (dec (get f1 (first s)))) (conj res :good))
                  (contient (first s) v1) (if (> (get f1 (first s)) 0)
                                              (recur (rest s) (inc i) (assoc f1 (first s) (dec (get f1 (first s)))) (conj res :color))
                                              (recur (rest s) (inc i) (assoc f1 (first s) (dec (get f1 (first s)))) (conj res :bad)))
                  :else (recur (rest s) (inc i) (assoc f1 (first s) 1) (conj res :bad)))
              res)))

;(indications [:vert :rouge :rouge :vert] [:rouge :jaune :vert :vert] {:vert 2, :rouge 2})

(defn ecrire_indications
  "affichage des indications"
  [v1 v2 f1]
  (loop [i 0]
    (when (< i 4)
       (print (str (name (nth (indications v1 v2 f1) i)) " "))
       (recur (inc i)))))

(defn ecrire_gagne
  "affichage en cas de reussite"
  [v1 v2 f1]
  (when (= (indications v1 v2 f1) [:good :good :good :good])
     (println "!!! BRAVO VOUS AVEZ GAGNE !!!")
     (System/exit 0)))

(defn ecrire_perdu
  "affichage en cas de defaite"
  []
  (println "DOMMMAGE, c'est PERDU")
  (System/exit 0))

(defn ecrire_couleurs
   "affichage explications"
   []
   (println "Ecrivez 4 couleurs (rouge, bleu, vert, jaune, noir, blanc)")
   (println "(sur 1 ligne et mots separes par un espace)"))

(defn ecrire_separation
    "affichage separation"
    []
    (println " ")
    (println "-----------------------"))

(defn introduction
  "affichage texte d'introduction"
  []
  (println "=================================================")
  (println "|                 MASTERMIND                    |")
  (println "|         (Benjamin Malinbaum 3674644)          |")
  (println "=================================================")
  (println "D : Vous etes le Decodeur")
  (println "Vous devez trouver le code compose de 4 couleurs, et cache par le Codificateur  -6 Couleurs, 12 essais")
  (println "C : Vous etes le Codeur")
  (println "Vous creez un code de 4 couleurs, et donnez des indications au Decodeur en fonction de ses reponses -good, bad, color")
  (println "--------------------------------")
  (println "Choisissez votre role (D ou C):")
  (println "--------------------------------"))
