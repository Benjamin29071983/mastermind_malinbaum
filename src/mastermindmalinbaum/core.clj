(ns mastermindmalinbaum.core
  (:require [mastermindmalinbaum.fonctions1 :refer :all])
  (:require [mastermindmalinbaum.fonctions2 :refer :all]))

(defn -main
  "Jeu du Mastermind"
  [& args]
  (introduction)

  (def jeu (read))

  (when (= (keyword jeu) :D)
        (generateur)
        [gen1 gen2 gen3 gen4]
        (ecrire_couleurs)
        (ecrire_separation)

        (loop [j 0]
          (when (< j 12)
            (let [entree1 (read) entree2 (read) entree3 (read) entree4 (read)]
              (def v2 [(keyword entree1) (keyword entree2) (keyword entree3) (keyword entree4)])
              ;(def f1 (frequences [gen1 gen2 gen3 gen4]))
              ;(def f2 (frequences v2))
              (ecrire_indications [gen1 gen2 gen3 gen4] v2 (frequences [gen1 gen2 gen3 gen4]))
              (ecrire_separation)
              (ecrire_gagne [gen1 gen2 gen3 gen4] v2 (frequences [gen1 gen2 gen3 gen4]))
              (recur (inc j)))))
        (ecrire_perdu))

  (println "Choisissez 4 couleurs, que l'Ordinateur va devoir trouver")
  (println "(rouge, bleu, vert, jaune, noir, blanc)")
  (ecrire_separation)
  (def couleur1 (read))
  (def couleur2 (read))
  (def couleur3 (read))
  (def couleur4 (read))
  (def vecteur_a_touver [couleur1 couleur2 couleur3 couleur4])
  (println "Quelle est votre choix 1, Mr Computer?")
  (ecrire_separation)

  (ecrire_vecteur [:rouge :bleu :vert :jaune])
  (ecrire_nos_indic)

  (loop [k 0]
   (when (< k 12)
      (let [indic1 (read)
            indic2 (read)
            indic3 (read)
            indic4 (read)
            vecteur_place [:none :none :none :none]]
        (def vecteur_indications [(keyword indic1) (keyword indic2) (keyword indic3) (keyword indic4)])
        (ecrire_vecteur1 (generateur_dyn vecteur_a_touver vecteur_indications vecteur_place))

        (ecrire_separation)
        (ecrire_gagne_partie2 vecteur_indications)
        (recur (inc k)))))
  (ecrire_perdu))
